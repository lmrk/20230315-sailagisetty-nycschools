//
//  SATDetails.swift
//  ChasePOC2
//
//  Created by Sai Lagisetty on 3/14/23.
//

import Foundation

struct SATDetails: Decodable, Identifiable {
    var id = UUID()
    let dbn: String?
    let schoolName: String?
    let numOfSatTestTakers: String?
    let satCriticalReadingAvgScore: String?
    let satWritingAvgScore: String?
    
    private enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName
        case numOfSatTestTakers
        case satCriticalReadingAvgScore
        case satWritingAvgScore
    }
}


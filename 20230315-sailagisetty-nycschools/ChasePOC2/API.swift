//
//  API.swift
//  ChasePOC2
//
//  Created by Sai Lagisetty on 3/14/23.
//

import Foundation

class API {
    let url: URL
    
    init(url: URL) {
        self.url = url
    }
    
    func perform<T: Decodable>(with completion: @escaping ((Result<T, APIRequestError>)) -> Void) {
        let session = URLSession(configuration: .default, delegate: nil, delegateQueue: .main)
        let task = session.dataTask(with: url) { (data, response, _) in
            
            guard let response = response as? HTTPURLResponse,
            let data = data else {
               return completion(.failure(.noResponse))
            }
            
            switch response.statusCode {
            case 200...299:
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                guard let decodedResponse = try? decoder.decode(T.self, from: data) else {
                   return completion(.failure(.decode))
                }
                completion(.success(decodedResponse))
            case 401:
                completion(.failure(.unauthorised))
            default:
                completion(.failure(.unknown))
            }
        }
        task.resume()
    }
}

enum APIRequestError: Error {
    case decode
    case invalidURL
    case noResponse
    case unauthorised
    case offline
    case unknown

    var customMessage: String {
        switch self {
        case .decode:
            return "Decode error"
        case .unauthorised:
            return "Session expired"
        default:
            return "Unknown error"
        }
    }
}
